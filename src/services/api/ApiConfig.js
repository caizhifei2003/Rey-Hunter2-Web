export default {
  account: {
    signin: { path: "account/signin", method: "POST", auth: false },
    signinEnterprise: { path: "account/signin/enterprise", method: "POST" },
    signup: { path: "account/signup", method: "POST", auth: false }
  },
  enterprise: {
    create: { path: "enterprise", method: "POST" },
    update: { path: "enterprise/{id}", method: "PUT" },
    delete: { path: "enterprise/{id}", method: "DELETE" },
    queryAll: { path: "enterprise/all", method: "GET" }
  },
  industry: {
    create: { path: "industry", method: "POST" },
    update: { path: "industry/{id}", method: "PUT" },
    delete: { path: "industry/{id}", method: "DELETE" },
    nodes: { path: "industry/nodes", method: "GET" }
  },
  function: {
    create: { path: "function", method: "POST" },
    update: { path: "function/{id}", method: "PUT" },
    delete: { path: "function/{id}", method: "DELETE" },
    nodes: { path: "function/nodes", method: "GET" }
  },
  location: {
    create: { path: "location", method: "POST" },
    update: { path: "location/{id}", method: "PUT" },
    delete: { path: "location/{id}", method: "DELETE" },
    nodes: { path: "location/nodes", method: "GET" }
  },
  category: {
    create: { path: "category", method: "POST" },
    update: { path: "category/{id}", method: "PUT" },
    delete: { path: "category/{id}", method: "DELETE" },
    nodes: { path: "category/nodes", method: "GET" }
  },
  channel: {
    create: { path: "channel", method: "POST" },
    update: { path: "channel/{id}", method: "PUT" },
    delete: { path: "channel/{id}", method: "DELETE" },
    nodes: { path: "channel/nodes", method: "GET" }
  },
  talent: {
    create: { path: "talent", method: "POST" },
    update: { path: "talent/{id}", method: "PUT" },
    delete: { path: "talent/{id}", method: "DELETE" },
    deleteBatch: { path: "talent", method: "DELETE" },
    get: { path: "talent/{id}", method: "GET" },
    query: { path: "talent", method: "GET" },
    queryPage: { path: "talent/page", method: "GET" }
  },
  company: {
    create: { path: "company", method: "POST" },
    update: { path: "company/{id}", method: "PUT" },
    delete: { path: "company/{id}", method: "DELETE" },
    deleteBatch: { path: "company", method: "DELETE" },
    get: { path: "company/{id}", method: "GET" },
    query: { path: "company", method: "GET" },
    queryPage: { path: "company/page", method: "GET" }
  },
  project: {
    create: { path: "project", method: "POST" },
    update: { path: "project/{id}", method: "PUT" },
    delete: { path: "project/{id}", method: "DELETE" },
    deleteBatch: { path: "project", method: "DELETE" },
    get: { path: "project/{id}", method: "GET" },
    query: { path: "project", method: "GET" },
    queryPage: { path: "project/page", method: "GET" }
  }
};
