import Api from "./Api";

export default {
  install: function(Vue, options) {
    var api = new Api(options);
    Vue.api = api;
    Vue.prototype.$api = api;
  }
};
