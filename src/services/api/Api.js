import Vue from "vue";
import ApiPath from "./ApiPath";
import ApiConfig from "./ApiConfig";

const __object_each = (obj, each) => {
  Object.keys(obj).forEach(key => {
    each(key, obj[key]);
  });
};

const __fetch_options = ({
  request: { method = "GET", auth = true, mode = "json" },
  args: { headers = {}, data = {} }
}) => {
  const options = {
    method,
    headers: { ...headers }
  };

  if (mode === "json") {
    options.headers = {
      ...options.headers,
      "Content-Type": "application/json"
    };
  } else if (mode === "form") {
    options.headers = {
      ...options.headers,
      "Content-Type": "application/x-www-form-urlencoded"
    };
  }

  if (auth) {
    const token = localStorage.getItem("token");
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${token}`
    };
  }

  if (method === "POST" || method === "PUT") {
    options.body = JSON.stringify(data);
  }

  return options;
};

const __fetch_path = ({ request: { path }, args: { query = {} } }) => {
  const last = [];
  __object_each(query, (key, value) => {
    if (!value) return;
    const reg = new RegExp(`{${key}}`);
    if (reg.test(path)) {
      path = path.replace(reg, () => value);
    } else {
      last.push({ key, value });
    }
  });

  if (last.length > 0) {
    const items = [];
    last.forEach(({ key, value }) => {
      if (Array.isArray(value)) {
        value.forEach((v, i) => items.push({ key: `${key}[${i}]`, value: v }));
        return;
      }
      items.push({ key, value });
    });

    const queryString = items
      .map(({ key, value }) => `${key}=${value}`)
      .join("&");
    const delimiter = "".indexOf("?") === -1 ? "?" : "&";
    path = `${path}${delimiter}${queryString}`;
  }
  return path;
};

export default class Api {
  constructor(options) {
    this._path = new ApiPath(options);

    __object_each(ApiConfig, (group_name, group) => {
      this[group_name] = {};
      __object_each(group, (request_name, request) => {
        this[group_name][request_name] = (args = {}) => {
          const options = __fetch_options({ request, args });
          const url = this._path.url(__fetch_path({ request, args }));
          return fetch(url, options)
            .then(resp => {
              return resp.json();
            })
            .then(resp => {
              const {
                data,
                error: { code, message }
              } = resp;
              if (code !== 0) {
                const error = new Error(`[code: ${code}][message: ${message}]`);
                error.code = code;
                error.message = message;
                throw error;
              }
              return data;
            });
        };
      });
    });
  }

  async signin(args) {
    const result = await this.account.signin(args);
    Vue.token.save(result);
    return true;
  }

  async signinEnterprise(args) {
    const result = await this.account.signinEnterprise(args);
    Vue.token.save(result);
    return true;
  }

  signup(args) {
    return this.account.signup(args);
  }
}
