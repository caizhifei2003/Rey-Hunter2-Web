export default class ApiPath {
  constructor({ schema = "http", host = "localhost", port = 80 } = {}) {
    this._schema = schema;
    this._host = host;
    this._port = port;
  }

  url(path) {
    const port =
      (this._port === 80 && this._schema === "http") ||
      (this._port === 443 && this._schema === "https")
        ? ""
        : `:${this._port}`;
    return `${this._schema}://${this._host}${port}/${path.replace(/^\/+/, "")}`;
  }
}
