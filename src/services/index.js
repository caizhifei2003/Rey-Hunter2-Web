import Vue from "vue";
import api from "./api";
import token from "./Token";

const { VUE_APP_API_SCHEMA, VUE_APP_API_HOST, VUE_APP_API_PORT } = process.env;

Vue.use(api, {
  schema: VUE_APP_API_SCHEMA,
  host: VUE_APP_API_HOST,
  port: parseInt(VUE_APP_API_PORT)
});

Vue.use(token);
