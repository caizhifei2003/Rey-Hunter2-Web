import jwt_decode from "jwt-decode";

const TOKEN = "token";
const TOKEN_EXPIRES_IN = "token_expires_in";
const TOKEN_EXPIRES_AT = "token_expires_at";

class Token {
  save({ token, expiresIn }) {
    localStorage.setItem(TOKEN, token);
    localStorage.setItem(TOKEN_EXPIRES_IN, expiresIn);
    localStorage.setItem(
      TOKEN_EXPIRES_AT,
      new Date().getTime() + expiresIn * 1000
    );
  }

  clear() {
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(TOKEN_EXPIRES_IN);
    localStorage.removeItem(TOKEN_EXPIRES_AT);
  }

  decode() {
    const token = localStorage.getItem(TOKEN);
    return jwt_decode(token);
  }

  expiresIn() {
    const expiresIn = localStorage.getItem(TOKEN_EXPIRES_IN);
    if (!expiresIn) return null;
    return parseInt(expiresIn);
  }

  authorized() {
    const token = localStorage.getItem(TOKEN);
    if (!token) return false;
    const expires_at = parseInt(localStorage.getItem(TOKEN_EXPIRES_AT));
    if (new Date().getTime() > expires_at) return false;
    return true;
  }

  enterpriseAuthorized() {
    if (!this.authorized()) return false;
    const { e_id = null } = this.decode();
    if (!e_id) return false;
    return true;
  }
}

export default {
  install: function(Vue) {
    var token = new Token();
    Vue.token = token;
    Vue.prototype.$token = token;
  }
};
