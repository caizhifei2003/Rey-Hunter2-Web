import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: () => import("./layouts/Default.vue"),
      children: [
        {
          path: "",
          redirect: "dashboard"
        },
        {
          path: "dashboard",
          name: "dashboard",
          component: () => import("./views/Dashboard.vue")
        },
        {
          path: "project",
          name: "project",
          component: () => import("./views/Project.vue")
        },
        {
          path: "talent",
          name: "talent",
          component: () => import("./views/Talent.vue")
        },
        {
          path: "company",
          name: "company",
          component: () => import("./views/Company.vue")
        },
        {
          path: "statistics",
          name: "statistics",
          component: () => import("./views/Statistics.vue")
        },
        {
          path: "setting",
          name: "setting",
          redirect: "setting/profile",
          component: () => import("./layouts/Setting.vue"),
          children: [
            {
              path: "profile",
              name: "profile",
              component: () => import("./views/setting/Profile.vue")
            },
            {
              path: "industry",
              name: "industry",
              component: () => import("./views/setting/data/Industry.vue")
            },
            {
              path: "function",
              name: "function",
              component: () => import("./views/setting/data/Function.vue")
            },
            {
              path: "location",
              name: "location",
              component: () => import("./views/setting/data/Location.vue")
            },
            {
              path: "category",
              name: "category",
              component: () => import("./views/setting/data/Category.vue")
            },
            {
              path: "channel",
              name: "channel",
              component: () => import("./views/setting/data/Channel.vue")
            },
            {
              path: "role",
              name: "role",
              component: () => import("./views/setting/authorization/Role.vue")
            },
            {
              path: "member",
              name: "member",
              component: () =>
                import("./views/setting/authorization/Member.vue")
            },
            {
              path: "changepassword",
              name: "changepassword",
              component: () =>
                import("./views/setting/security/ChangePassword.vue")
            },
            {
              path: "notification",
              name: "notification",
              component: () => import("./views/setting/Notification.vue")
            }
          ]
        }
      ]
    },
    {
      path: "/account",
      component: () => import("./layouts/Account.vue"),
      children: [
        {
          path: "signup",
          name: "signup",
          component: () => import("./views/account/SignUp.vue")
        },
        {
          path: "signin",
          name: "signin",
          component: () => import("./views/account/SignIn.vue")
        }
      ]
    },
    {
      path: "/enterprise",
      name: "enterprise",
      component: () => import("./views/Enterprise.vue")
    }
  ]
});
