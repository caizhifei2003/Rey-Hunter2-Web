import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import colors from "vuetify/es5/util/colors";

Vue.use(Vuetify, {
  theme: {
    // primary: colors.blueGrey, 
    // secondary: colors.red.lighten4, // #FFCDD2
    // accent: colors.indigo.base // #3F51B5
  }
});
