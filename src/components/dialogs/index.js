import Vue from "vue";
import DeleteDialog from "./DeleteDialog";
import TalentDialog from "./TalentDialog";
import NodeDialog from "./NodeDialog";
import NodeSelectDialog from "./NodeSelectDialog";
import EnterpriseDialog from "./EnterpriseDialog";
import CompanyDialog from "./CompanyDialog";
import CompanySelectDialog from "./CompanySelectDialog";
import ProjectDialog from "./ProjectDialog";

Vue.component("app-dialog-delete", DeleteDialog);
Vue.component("app-dialog-talent", TalentDialog);
Vue.component("app-dialog-node", NodeDialog);
Vue.component("app-dialog-select-node", NodeSelectDialog);
Vue.component("app-dialog-enterprise", EnterpriseDialog);
Vue.component("app-dialog-company", CompanyDialog);
Vue.component("app-dialog-select-company", CompanySelectDialog);
Vue.component("app-dialog-project", ProjectDialog);
