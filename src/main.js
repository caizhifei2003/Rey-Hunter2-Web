import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./third";
import "./components";
import "./services";

console.log(process.env.NODE_ENV);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
